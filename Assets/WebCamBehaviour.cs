﻿using System;
using UnityEngine;
using System.Collections.Generic;
using VRStandardAssets.Utils;
using UnityEngine.UI;
using System.Collections;

public class WebCamBehaviour : MonoBehaviour
{
    public enum Effects
    {
        None = 0,
        SensitivityToLight = 1,
        FloatingSpots = 2,
        Disorientation = 3,
        ThumpingHeadache = 4,
        Aura = 5
    }
    public enum States
    {
        NotPlaying = 0,
        Playing = 1
    }
    public List<Texture> distortionMaps;

    /// <summary>
    /// Meta reference to the camera
    /// </summary>
    public Material CameraMaterial;

    public RawImage uiImage;

    /// <summary>
    /// The number of frames per second
    /// </summary>
    private int m_framesPerSecond = 0;

    /// <summary>
    /// The current frame count
    /// </summary>
    private int m_frameCount = 0;

    /// <summary>
    /// The frames timer
    /// </summary>
    private DateTime m_timerFrames = DateTime.MinValue;

    /// <summary>
    /// The selected device index
    /// </summary>
    private int m_indexDevice = -1;

    /// <summary>
    /// The web cam texture
    /// </summary>
    private WebCamTexture m_texture = null;

    private Quaternion baseRotation;

    private int m_indexMap = -1;

    public Shader designatedShader;

    //public Material designatedMaterial;

    public bool isStarted;

    private RenderTexture resultTexture;

    public VRInput m_VRInput;

    public Text output;
    public Text debugText;

    public States currentState;
    public Effects currentEffect;

    public GameObject FlakesEffect;
    public GameObject RingEffect;
    public float effectDuration;

    public MenuHandler myMenuHandler;

    bool cameraInitialized;
    bool isRotating;
    float _angle = 0f;
    float _minAngle = 0f;
    float _maxAngle = 0.2f;
    float _timer = 0;

    void OnEnable()
    {
        m_VRInput.OnClick += HandleClick;
        m_VRInput.OnCancel += HandleCancel;

        if (cameraInitialized)
        {
            currentEffect = Effects.None;
            currentState = States.NotPlaying;
            FormatEffect();
            NextEffectContainer();
        }
    }

    void OnDisable()
    {
        m_VRInput.OnClick -= HandleClick;
        m_VRInput.OnCancel -= HandleCancel;
        currentEffect = Effects.None;
        currentState = States.NotPlaying;
        FormatEffect();
    }

    // Use this for initialization
    void Start()
    {
        //distortionMaps.Add(null);
        isStarted = false;
        cameraInitialized = false;
        isRotating = false;
        _timer = 0;

        currentEffect = Effects.None;
        currentState = States.NotPlaying;

        if (null == CameraMaterial)
        {
            throw new ApplicationException("Missing camera material reference");
        }
        if (null == uiImage)
        {
            throw new ApplicationException("Missing ui raw image reference");
        }

        Application.RequestUserAuthorization(UserAuthorization.WebCam);
        //baseRotation = GetComponent<RectTransform>().localRotation;
        Invoke("NextCamera", 5.0f);
        Invoke("NextEffectContainer", 6.0f);
        //Invoke("NextMap", 10.0f);
    }

    void OnGUI()
    {
        //if (m_texture != null)
        //{
        //    string value = m_texture.videoRotationAngle.ToString();
        //    GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 20, 20), value);

        //    //if (GUI.Button(new Rect(200, 70, 50, 30), "Snap"))
        //    //{
        //    //    TakeSnapshot();
        //    //}
        //}

    }

    // Update is called once per frame

    private void Update()
    {

        if (m_texture != null && m_texture.didUpdateThisFrame && m_texture.isPlaying /*&& !isOriented*/)
        {
            //GetComponent<RectTransform>().localRotation = baseRotation * Quaternion.AngleAxis(m_texture.videoRotationAngle - 90, Vector3.up);
            //Debug.Log("Updated with angle: " + m_texture.videoRotationAngle);
            //isOriented = true;
            if (m_texture.width < 100)
            {
                Debug.Log("Still waiting another frame for correct info...");
                return;
            }
            else
            {
                //if (!cameraInitialized)
                //{
                Debug.Log("boop!");
                cameraInitialized = true;
                // change as user rotates iPhone or Android:

                int cwNeeded = m_texture.videoRotationAngle;
                // Unity helpfully returns the _clockwise_ twist needed
                // guess nobody at Unity noticed their product works in counterclockwise:
                int ccwNeeded = -cwNeeded;

                // IF the image needs to be mirrored, it seems that it
                // ALSO needs to be spun. Strange: but true.
                if (m_texture.videoVerticallyMirrored) ccwNeeded += 180;

                // you'll be using a UI RawImage, so simply spin the RectTransform
                uiImage.rectTransform.localEulerAngles = new Vector3(0f, 0f, ccwNeeded);
                //angle.text = ccwNeeded.ToString();

                float videoRatio = (float)m_texture.width / (float)m_texture.height;

                // you'll be using an AspectRatioFitter on the Image, so simply set it
                uiImage.gameObject.GetComponent<AspectRatioFitter>().aspectRatio = videoRatio;

                // alert, the ONLY way to mirror a RAW image, is, the uvRect.
                // changing the scale is completely broken.
                if (m_texture.videoVerticallyMirrored)
                {
                    uiImage.uvRect = new Rect(1, 0, -1, 1);  // means flip on vertical axis
                    //uiImage.uvRect = new Rect(0, 1, 1, -1);  // flip on HORIZONTAL axis
                }

                else
                {
                    uiImage.uvRect = new Rect(0, 0, 1, 1);  // means no flip
                }


                //if (canvasTransform == null)
                RectTransform canvasTransform = GetComponentInParent<Canvas>().gameObject.transform as RectTransform;

                RectTransform rt = transform as RectTransform;
                rt.sizeDelta = new Vector2(canvasTransform.sizeDelta.y, canvasTransform.sizeDelta.y / videoRatio);
                FormatEffect();
                //debugText.text = (m_texture.width + " " + m_texture.height + rt.sizeDelta.x + " " + rt.sizeDelta.y);
                //Debug.Log(m_texture.width + " " + m_texture.height);
                //}


            }



        }

        //tweening values
        if (isRotating)
        {
            // animate the position of the game object...
            _angle = Mathf.Lerp(_minAngle, _maxAngle, _timer);
            CameraMaterial.SetFloat("_Angle", _angle);
            // .. and increate the t interpolater
            _timer += 0.5f * Time.deltaTime;

            // now check if the interpolator has reached 1.0
            // and swap maximum and minimum so game object moves
            // in the opposite direction.
            if (_timer >= 1.0f)
            {
                float temp = _maxAngle;
                _maxAngle = _minAngle;
                _minAngle = temp;
                _timer = 0.0f;
            }

        }

    }

    public event Action EnableCam;

    void NextCamera()
    {
        if (WebCamTexture.devices != null)
        {
            if (m_indexDevice == WebCamTexture.devices.Length - 1)
            {
                m_indexDevice = 0;
            }
            else
            {
                m_indexDevice++;
            }
            // stop playing
            if (null != m_texture)
            {
                if (m_texture.isPlaying)
                {
                    m_texture.Stop();
                    isStarted = false;
                }
            }

            // destroy the old texture
            if (null != m_texture)
            {
                UnityEngine.Object.DestroyImmediate(m_texture, true);
            }

            // use the device name
            m_texture = new WebCamTexture(WebCamTexture.devices[m_indexDevice].name);

            // add webcam texture to distortion shader
            CameraMaterial.SetTexture("_MainTex", m_texture);
            CameraMaterial.SetTextureScale("_MainTex", new Vector2(1f, 1f));
            m_indexMap = -1;


            // start playing
            PlayCamera();

            // assign the texture
            uiImage.texture = m_texture;
            CameraMaterial.mainTexture = m_texture;
        }

    }

    void PlayCamera()
    {
        m_texture.Play();
    }

    // For saving to the _savepath
    private string _SavePath = "C:/"; //Change the path here!
    int _CaptureCounter = 0;
    void TakeSnapshot()
    {
        Debug.Log("Picture: " + m_texture.width + "x" + m_texture.height);


        var myMaterial = Instantiate(GetComponent<MeshRenderer>().material);

        resultTexture = new RenderTexture(m_texture.width, m_texture.height, 0);

        Graphics.Blit(m_texture, resultTexture, myMaterial);


        Texture2D snapShot = new Texture2D(m_texture.height, m_texture.width, TextureFormat.RGBA32, false);

        RenderTexture.active = resultTexture;

        Texture2D tempSS = new Texture2D(m_texture.width, m_texture.height, TextureFormat.RGBA32, false);

        tempSS.ReadPixels(new Rect(0, 0, resultTexture.width, resultTexture.height), 0, 0);
        //snapShot.SetPixels(rotateTextureGrid(m_texture.GetPixels(), m_texture.width, m_texture.height));

        snapShot.SetPixels(rotateTextureGrid(tempSS.GetPixels(), tempSS.width, tempSS.height));
        snapShot.Apply();

        System.IO.File.WriteAllBytes(_CaptureCounter.ToString() + ".png", snapShot.EncodeToPNG());
        ++_CaptureCounter;
        RenderTexture.active = null;
    }

    public Color[] rotateTextureGrid(Color[] tex, int wid, int hi)
    {
        Color[] ret = new Color[wid * hi];      //reminder we are flipping these in the target

        for (int y = 0; y < hi; y++)
        {
            for (int x = 0; x < wid; x++)
            {
                ret[y + (wid - 1 - x) * hi] = tex[x + y * wid];         //juggle the pixels around

            }
        }

        return ret;
    }

    IEnumerator ApplyMap()
    {
        //new implementation
        CameraMaterial.SetTexture("_DistortionMapRG", distortionMaps[((int)currentEffect) - 2]);
        yield return new WaitForSeconds(effectDuration);
        NextEffectContainer();
    }

    void ClearMap()
    {
        CameraMaterial.SetTexture("_DistortionMapRG", null);
        //m_indexMap = distortionMaps.Count - 1;
    }

    IEnumerator StartBrightness()
    {
        Debug.Log("started brightness");
        isStarted = true;
        CameraMaterial.SetFloat("_RunBrightness", 1f);
        yield return new WaitForSeconds(effectDuration);
        NextEffectContainer();
    }

    void StopBrightness()
    {
        CameraMaterial.SetFloat("_RunBrightness", 0f);
        Debug.Log("ended brightness");
    }

    IEnumerator StartRotation()
    {
        CameraMaterial.SetFloat("_RunRotation", 1f);
        isRotating = true;
        _timer = 0;
        yield return new WaitForSeconds(effectDuration);
        NextEffectContainer();
    }

    void StopRotation()
    {
        CameraMaterial.SetFloat("_RunRotation", 0f);
        isRotating = false;
        _timer = 0;
        _angle = 0;
    }

    IEnumerator StartFlakes()
    {
        FlakesEffect.SetActive(true);
        yield return new WaitForSeconds(effectDuration);
        NextEffectContainer();
    }

    void StopFlakes()
    {
        FlakesEffect.SetActive(false);
    }

    IEnumerator StartRing()
    {
        RingEffect.SetActive(true);
        RingEffect.GetComponent<ScalingBehaviour>().StartScaling();
        yield return new WaitForSeconds(effectDuration);
        NextEffectContainer();
    }

    void StopRing()
    {
        RingEffect.SetActive(false);
        RingEffect.GetComponent<ScalingBehaviour>().StopScaling();
    }

    IEnumerator StartAfter(string fCoroutine, float fTime)
    {
        Debug.Log("started timer");
        isStarted = true;
        yield return new WaitForSeconds(fTime);
        Debug.Log("ended timer");
        StartCoroutine(fCoroutine);
    }

    IEnumerator StartAura()
    {
        GetComponent<ScorllUV>().StartScrolling();
        yield return new WaitForSeconds(effectDuration);
        NextEffectContainer();
    }

    void StopAura()
    {
        GetComponent<ScorllUV>().StopScrolling();
    }

    void FormatEffect()
    {
        Debug.Log("Format Effects");
        switch (currentEffect)
        {
            case Effects.None:
                output.text = "No Effect Selected";
                break;
            case Effects.SensitivityToLight:
                output.text = "Sensitivity to Light";
                break;
            case Effects.FloatingSpots:
                output.text = "Floating Spots";
                break;
            case Effects.ThumpingHeadache:
                output.text = "Thumping Headache";
                break;
            case Effects.Disorientation:
                output.text = "Disorientation";
                break;
            case Effects.Aura:
                output.text = "Aura";
                break;
            default:
                break;
        }
    }

    void StopEffects()
    {
        Debug.Log("Stop Effects");
        StopAllCoroutines();

        switch (currentEffect)
        {
            case Effects.None:
                break;
            case Effects.SensitivityToLight:
                StopBrightness();
                currentState = States.NotPlaying;
                break;
            case Effects.FloatingSpots:
                StopFlakes();
                currentState = States.NotPlaying;
                break;
            case Effects.ThumpingHeadache:
                StopRing();
                currentState = States.NotPlaying;
                break;
            case Effects.Disorientation:
                StopRotation();
                currentState = States.NotPlaying;
                break;
            case Effects.Aura:
                StopAura();
                currentState = States.NotPlaying;
                break;
            default:
                //ClearMap();
                //currentState = States.NotPlaying;
                break;
        }

    }

    void NextEffect()
    {
        Debug.Log("Next Effect");
        int currentEffectIndex = (int)currentEffect;
        var tempValues = Enum.GetValues(typeof(Effects));
        if (currentEffectIndex == tempValues.Length - 1)
        {
            currentEffectIndex = 1;
        }
        else
        {
            currentEffectIndex++;
        }
        currentEffect = (Effects)currentEffectIndex;
    }

    void InitiateEffect()
    {
        Debug.Log("Initiate Effect");
        StopAllCoroutines();
        switch (currentEffect)
        {
            case Effects.None:
                break;
            case Effects.SensitivityToLight:
                StartCoroutine("StartBrightness");
                currentState = States.Playing;
                break;
            case Effects.FloatingSpots:
                StartCoroutine("StartFlakes");
                currentState = States.Playing;
                break;
            case Effects.ThumpingHeadache:
                StartCoroutine("StartRing");
                currentState = States.Playing;
                break;
            case Effects.Disorientation:
                StartCoroutine("StartRotation");
                currentState = States.Playing;
                break;
            case Effects.Aura:
                StartCoroutine("StartAura");
                currentState = States.Playing;
                break;
            default:
                //StartCoroutine("ApplyMap");
                //currentState = States.Playing;
                break;
        }
    }

    void HandleClick()
    {
        //switch (currentState)
        //{
        //    case States.NotPlaying:
        //        switch (currentEffect)
        //        {
        //            default:
        //                InitiateEffect();
        //                break;
        //        }
        //        break;
        //    case States.Playing:
        //        StopEffects();
        //        break;
        //    default:
        //        break;
        //}
    }

    void HandleCancel()
    {
        StopEffects();
        myMenuHandler.GotoMenu();
        //switch (currentState)
        //{
        //    case States.NotPlaying:
        //        NextEffect();
        //        FormatEffect();
        //        InitiateEffect();
        //        break;
        //    case States.Playing:
        //        StopEffects();
        //        break;
        //    default:
        //        break;
        //}

    }

    void NextEffectContainer()
    {
        StopEffects();
        NextEffect();
        FormatEffect();
        InitiateEffect();
    }
}