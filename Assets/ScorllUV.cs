﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorllUV : MonoBehaviour
{

    //scroll main texture based on time
    /// <summary>
    /// Meta reference to the camera
    /// </summary>
    public Material CameraMaterial;

    public Texture distortionMap;

    float scrollSpeed = 0.5f;
    public float minOffset = -1f;
    public float maxOffset = 1f;
    public float offset = 0;
    public float timer = 0;
    float rotate;

    bool isScrolling = false;

    void Update()
    {
        //offset = Mathf.Lerp(minOffset,maxOffset)
        //offset += (Time.deltaTime * scrollSpeed) / 10.0f;
        //GetComponent<Renderer>().material.SetTextureOffset("_DistortionMapRG", new Vector2(offset, 0));


        //tweening values
        if (isScrolling)
        {
            // animate the position of the game object...
            offset = Mathf.Lerp(minOffset, maxOffset, timer);
            CameraMaterial.SetTextureOffset("_DistortionMapRG", new Vector2(offset, 0));
            // .. and increate the t interpolater
            timer += 0.5f * Time.deltaTime;

            // now check if the interpolator has reached 1.0
            // and swap maximum and minimum so game object moves
            // in the opposite direction.
            if (timer >= 1.0f)
            {
                float temp = maxOffset;
                maxOffset = minOffset;
                minOffset = temp;
                timer = 0.0f;
            }

        }
    }

    public void StartScrolling()
    {
        CameraMaterial.SetTexture("_DistortionMapRG", distortionMap);
        //GetComponent<Renderer>().material.SetTexture("_DistortionMapRG", distortionMap);
        isScrolling = true;
    }

    public void StopScrolling()
    {
        isScrolling = false;
        CameraMaterial.SetTexture("_DistortionMapRG", null);
        timer = 0f;
        offset = 0.25f;
    }
}
