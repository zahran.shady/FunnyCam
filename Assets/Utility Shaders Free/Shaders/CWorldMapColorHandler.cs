﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class CWorldMapColorHandler //: CSingleton<CWorldMapColorHandler>
{
    //public Shader m_TargetShader;
    //public List<Material> m_MaterialList = new List<Material>();

    //[Range(0f, 1f)]
    //public float
    //    m_Hue = 0f,
    //    m_Saturation = 0.5f,
    //    m_Brightness = 0.5f,
    //    m_Contrast = 0.5f;

    //HSBC m_Last;
    //HSBC m_Current { get { return new HSBC(m_Hue, m_Saturation, m_Brightness, m_Contrast); } }

    //[ContextMenu("Register Child Renderer")]
    //void OnValidate()
    //{
    //    // Ensure all related material reference in the list.
    //    List<MeshRenderer> renders = GetComponentsInChildren<MeshRenderer>().ToList();
    //    foreach (MeshRenderer render in renders)
    //    {
    //        for (int i = 0; i < render.sharedMaterials.Length; i++)
    //        {
    //            if (render.sharedMaterials[i] == null || render.sharedMaterials[i].shader.GetInstanceID() != m_TargetShader.GetInstanceID())
    //                continue;
    //            if (!m_MaterialList.Any(o => o.GetHashCode() == render.sharedMaterials[i].GetHashCode()))
    //                m_MaterialList.Add(render.sharedMaterials[i]);
    //        }
    //    }
    //}

    //new void Awake()
    //{
    //    base.Awake();
    //    m_Last = new HSBC(m_Hue, m_Saturation, m_Brightness, m_Contrast);
    //}

    //void Update()
    //{
    //    if (!m_Current.Equals(m_Last))
    //    {
    //        m_Last.LerpTo(m_Current, Time.deltaTime);
    //        // direct change Material values, 3 loops, change sharedMaterials 237 loops
    //        foreach (Material mat in m_MaterialList)
    //        {
    //            mat.SetFloat("_Hue", m_Last.Hue);
    //            mat.SetFloat("_Saturation", m_Last.Saturation);
    //            mat.SetFloat("_Brightness", m_Last.Brightness);
    //            mat.SetFloat("_Contrast", m_Last.Contrast);
    //        }
    //    }
    //}

    //new void OnDestroy()
    //{
    //    base.OnDestroy();

    //    // reset material color.
    //    foreach (Material mat in m_MaterialList)
    //    {
    //        mat.SetFloat("_Hue", 0f);
    //        mat.SetFloat("_Saturation", .5f);
    //        mat.SetFloat("_Brightness", .5f);
    //        mat.SetFloat("_Contrast", .5f);
    //    }
    //}
    //struct HSBC : IEquatable<HSBC>
    //{
    //    public float Hue, Saturation, Brightness, Contrast;
    //    public HSBC(float hue, float saturation, float brightness, float contrast)
    //    {
    //        Hue = hue;
    //        Saturation = saturation;
    //        Brightness = brightness;
    //        Contrast = contrast;
    //    }

    //    public bool Equals(HSBC other)
    //    {
    //        return
    //            Mathf.Approximately(Hue, other.Hue) &&
    //            Mathf.Approximately(Saturation, other.Saturation) &&
    //            Mathf.Approximately(Brightness, other.Brightness) &&
    //            Mathf.Approximately(Contrast, other.Contrast);
    //    }

    //    public void LerpTo(HSBC target, float t)
    //    {
    //        Hue = LerpNear(Hue, target.Hue, t);
    //        Saturation = LerpNear(Saturation, target.Saturation, t);
    //        Brightness = LerpNear(Brightness, target.Brightness, t);
    //        Contrast = LerpNear(Contrast, target.Contrast, t);
    //    }

    //    private float LerpNear(float current, float target, float t)
    //    {
    //        return (Mathf.Approximately(current, target)) ? target : Mathf.Lerp(current, target, t);
    //    }
    //}
}