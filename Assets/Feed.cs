﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Feed : MonoBehaviour
{
    public Shader myShader;
    public Material targetMaterial;
    public List<Texture> distortionMaps;
    public Texture distortionMap;
    public Texture myMainTexture;
    public Material myMainMaterial;
    private WebCamTexture webcam;
    private Quaternion baseRotation;
    private int currentIndex = 0;
    void Start()
    {


        

        
    }

    void Update()
    {
        if (webcam != null)
        {
            //transform.rotation = baseRotation * Quaternion.AngleAxis(webcam.videoRotationAngle, Vector3.up);
        }
        if (Input.GetMouseButtonDown(0))
        {
            //ApplyNextMap(null);
            PrintDevices();
            InitiateCam("Remote Camera 1");
            webcam.Play();

        }
    }

    void InitiateMap()
    {
        targetMaterial.SetTexture("_DistortionMapRG", distortionMaps[currentIndex]);
    }

    void ApplyNextMap(Lean.Touch.LeanFinger myFinger)
    {
        //if (currentIndex == distortionMaps.Count - 1)
        //{
        //    currentIndex = 0;
        //}
        //else
        //{
        //    currentIndex++;
        //}
        Debug.Log("swipe");

        targetMaterial.SetTexture("_DistortionMapRG", distortionMaps[currentIndex]);
    }

    void PrintDevices()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        foreach (var item in devices)
        {
            Debug.Log(item.name);
        }
    }

    void InitiateCam(string fCameraName)
    {
        webcam = new WebCamTexture(fCameraName);
        myMainTexture = GetComponent<MeshRenderer>().material.mainTexture;
        myMainMaterial = GetComponent<MeshRenderer>().material;

        baseRotation = transform.rotation;
        targetMaterial.shader = myShader;
        targetMaterial.SetTexture("_Texture", webcam);
        targetMaterial.SetTexture("_DistortionMapRG", null);

        GetComponent<MeshRenderer>().material = targetMaterial;
    }
}
