﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScalingBehaviour : MonoBehaviour
{

    RawImage myImage;
    RectTransform myTransform;
    Vector3 startScale;
    public Vector3 targetScale;
    public float step;
    public RippleEffect myRippleEffect;
    public float whenToRipple;

    float _timer;
    bool _ripple;
    // Use this for initialization
    void Awake()
    {
        myImage = GetComponent<RawImage>();
        myTransform = GetComponent<RectTransform>();
        startScale = myTransform.localScale;
        _timer = 0;
        _ripple = false;
    }

    public void StopScaling()
    {
        StopAllCoroutines();
        ResetScaling();
    }
    void ResetScaling()
    {
        myTransform.localScale = startScale;
        _ripple = false;
    }


    IEnumerator ScaleDown()
    {
        Vector3 tempScale;
        for (float s = startScale.x; s > targetScale.x; s -= step)
        {
            if (s / (startScale.x - targetScale.x) <= whenToRipple)
            {
                if (!_ripple)
                {
                    myRippleEffect.Emit();
                    //myRippleEffect.Emit();
                    _ripple = true;
                }
            }

            tempScale = new Vector3(s, s, s);
            myTransform.localScale = tempScale;
            yield return null;
        }
        ResetScaling();
        StartScaling();
    }

    public void StartScaling()
    {
        StartCoroutine("ScaleDown");
    }
}
