﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    List<Sprite> timerSprites;
    public GameObject targetObject;
    public GameObject targetWhiteEffect;
    public float enlargePercentage;
    public float timeToEnlarge;
    private Image myImage;
    private IEnumerator myCoroutine;
    // Use this for initialization
    void Start()
    {
        myImage = targetObject.GetComponent<Image>();
        myCoroutine = NumberEnlarge(targetObject, targetWhiteEffect, enlargePercentage, timeToEnlarge);
        StopAllCoroutines();
        StartCoroutine(myCoroutine);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator NumberEnlarge(GameObject fTargetTimer, GameObject fTargetWhiteEffect, float fEnlargePercentage, float fTimeToEnlarge)
    {
        //Debug.Log("coroutine started");
        RectTransform targetRectTransform = fTargetTimer.GetComponent<RectTransform>();
        Image targetWhiteEffectImage = fTargetWhiteEffect.GetComponent<Image>();

        Vector2 startSize = targetRectTransform.sizeDelta;
        float startWidth = targetRectTransform.sizeDelta.x;
        float startHeight = targetRectTransform.sizeDelta.y;

        float targetWidth = startWidth * ((fEnlargePercentage / 100f) + 1f);
        float targetHeight = startHeight * ((fEnlargePercentage / 100f) + 1f);

        Vector2 targetSize = new Vector2(targetWidth, targetHeight);

        Vector2 whiteEffectStartSize = fTargetWhiteEffect.GetComponent<RectTransform>().sizeDelta;
        float whiteEffectStartWidth = fTargetWhiteEffect.GetComponent<RectTransform>().sizeDelta.x;
        float whiteEffectStartHeight = fTargetWhiteEffect.GetComponent<RectTransform>().sizeDelta.y;

        float whiteEffectTargetWidth = whiteEffectStartWidth * ((fEnlargePercentage / 100f) + 1f);
        float whiteEffectTargetHeight = whiteEffectStartHeight * ((fEnlargePercentage / 100f) + 1f);

        Vector2 whiteEffectTargetSize = new Vector2(whiteEffectTargetWidth, whiteEffectTargetHeight);

        float startAlpha = targetWhiteEffectImage.color.a;
        float targetAlpha = 0;


        //float star

        //var currentPos = transform.position;
        var t = 0f;
        while (t < fTimeToEnlarge)
        {
            t += Time.deltaTime / fTimeToEnlarge;
            fTargetTimer.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(startSize, targetSize, t);
            fTargetWhiteEffect.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(whiteEffectStartSize, whiteEffectTargetSize, t);
            targetWhiteEffectImage.color = new Color(targetWhiteEffectImage.color.r, targetWhiteEffectImage.color.g, targetWhiteEffectImage.color.b, Mathf.Lerp(startAlpha, targetAlpha, t));
            yield return null;
        }
        yield return new WaitForSeconds(0.2f);
        fTargetTimer.SetActive(false);
        fTargetWhiteEffect.SetActive(false);
    }
}
