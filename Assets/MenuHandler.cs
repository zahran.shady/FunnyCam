﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

public class MenuHandler : MonoBehaviour
{
    public GameObject Menu, Effects;
    public VRInput m_VRInput;

    void OnEnable()
    {
        m_VRInput.OnClick += HandleClick;
        m_VRInput.OnCancel += HandleCancel;
    }

    void OnDisable()
    {
        m_VRInput.OnClick -= HandleClick;
        m_VRInput.OnCancel -= HandleCancel;
    }

    void HandleClick()
    {
        Effects.SetActive(true);
        Menu.SetActive(false);
    }

    void HandleCancel()
    {
        if(!Effects.activeInHierarchy)
        {
            Application.Quit();
        }
    }

    public void GotoMenu()
    {
        Menu.SetActive(true);
        Effects.SetActive(false);
    }

    public void GotoEffects()
    {
        Effects.SetActive(true);
        Menu.SetActive(false);
    }
}
